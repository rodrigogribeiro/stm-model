%$Id: figuide.tex,v 2.0 2016/11/04 by szczuka@mimuw.edu.pl$
\documentclass{fundam}
\usepackage{url} % takes care of hyperlinks, preferred over hyperref
\usepackage[ruled,lined]{algorithm2e}% provides Algorithm environment
\usepackage{graphicx}% allows for inclusion of graphic files (figures)

\begin{document}
\setcounter{page}{1001}
\issue{XXI~(2001)}

\title{How to Prepare Articles for Fundamenta Informaticae}

\address{Address for correspondence goes here}

\author{First Author\thanks{Thanks for something to somebody}\\
Institute of Informatics \\
University of Warsaw \\ Banacha 2, 02-097 Warszawa, Poland\\
first{@}mimuw.edu.pl
\and Second Author\thanks{Thanks for something else to somebody else}\\
Department of Informatics \\
City University \\
London, England } \maketitle

\runninghead{F. Author, S. Author}{How to Prepare Articles for FI}

\begin{abstract}
  This guide is for authors who are preparing papers for
  \emph{Fundamenta Informaticae\/} using the \LaTeXe\ document
  preparation system and the \texttt{fundam.cls} class file.
\end{abstract}

\begin{keywords}
\LaTeXe, document class file, Bib\TeX
\end{keywords}


\section{Introduction}

The \emph{Fundamenta Informaticae\/} class file is based on the
standard \LaTeXe\ \texttt{article} class as described in the \LaTeX\
manual.  It means that the paper prepared to be process with the
standard \texttt{article} class can be processed with the
\texttt{fundam} class without significant modifications.  Commands which
differ from the standard \LaTeX\ interface, or which are provided in
addition to the standard interface, are explained in this guide. This
guide is not a substitute for the \LaTeX\ manual itself.

The \texttt{fundam} class can be used only with \LaTeXe. It is not
supposed to work with the obsolete 2.09 (or earlier) version of
\LaTeX. The preferred format to be used is pdf\LaTeX\  (compilation directly to PDF).


The newest version of \emph{Fundamenta Informaticae\/} class (\texttt{fundam.cls} v. 2.0) as well as the newest version of bibliography style (\texttt{fundam.bst}) have been published in December 2016. If you have an earlier version of these files, you should update immediately. Version 2.0 of the class sets by default the page size to 262 by 192 mm, which is the actual page size of printed \emph{Fundamenta Informaticae\/}.

\subsection{The \texttt{fundam} document class}

The use of document styles allows a simple change of style (or style
option) to transform the appearance of the document.  The
\texttt{fundam} class file preserves the standard \LaTeX\ interface
such that any document which can be produced using the standard
\LaTeX\ \texttt{article} class, can also be produced with the
\texttt{fundam} style.  However, the measure (length and width of text) is
different to that used in \texttt{article} therefore line breaks will
change and long equations may need re-setting.

The authors are encouraged to minimize the manual line and page breaks
in the manuscripts.

It is essential that you do not modify \texttt{fundam.cls}. When
additional macros are required, place them in a separate file with the
filename extension \texttt{.sty} (e.g. \texttt{mymacros.sty}) and load it
as additional package file:
\begin{verbatim}
  \usepackage{mymacros}
\end{verbatim}
In this way you will distinguish clearly between the main style file
and your own macros, which can then be found easily by the journal editor.
Do not forget to submit your optional style file for publication along
with your input file.


\section{Using the \texttt{fundam} class}

First, copy the file \texttt{fundam.cls} (and \texttt{fundam.bst} if
you are using Bib\TeX) into the correct subdirectory on your system.
In order to use the \textbf{fundam} class, place it as the argument of
the \verb|\documentclass| command at the beginning of your
document. That is,
\begin{verbatim}
  \documentclass{article}
\end{verbatim}
is replaced by
\begin{verbatim}
  \documentclass{fundam}
\end{verbatim}

\noindent
Do not use any document style options.

The class uses macros from standard \LaTeXe\ packages:
\texttt{theorem} and \texttt{amsmath}. The page layout is set using two more  standard \LaTeXe\ packages: \texttt{geometry} and \texttt{fancyhdr}. These packages are included in
every recent \LaTeXe\ distribution.

\subsection{Title, authors and other meta information}

The title of the paper should be specified with the standard
\verb|\title| command.  The names and affiliation and address
information of the authors should be specified using \verb|\author|
command. The consecutive authors should be separated by \verb|\and|
command. The information for every author can contain more than one
line. The lines should be separated by \verb|\\| command. The first
line is regarded as the name of the author, next lines should contain
address/affiliation information.

It is possible to place the authors in two columns -- it can be useful
when the paper has three, four of more authors. Two column placement
of author information is activated after using the
\texttt{manyauthors} option in the invocation of class:

\begin{verbatim}
\documentclass[manyauthors]{fundam}
\end{verbatim}

The acknowledgement information can be assigned to authors using
standard \verb|\thanks| command placed after the name of the author
(inside \verb|\author| command). If two authors have the same
acknowledgement information, than the first author should be marked
with \verb|\thanks| command, the others should be marked with the
\verb|\thanksas{<number>}|. The examples of using these commands are
shown below.

The corresponding author can be marked with the \verb|\corresponding|
command placed after his name. This command produce the footnote
marked with the capital ``C'' as the superscript and the text:
``Corresponding author'' -- it means that the paper should be sent to
the marked author for corrections.

Optionally the correspondence address can be specified separately with
\verb|\address| command placed after \verb|\maketitle| command. The
example of the beginning of the paper written by the four authors is
shown below.

\begin{verbatim}
\documentclass[manyauthors]{fundam}
\begin{document}
\title{The example paper}
\author{First Author\thanks{Thanks for something}\\
  Institute of Informatics \\
  University of Somewere
\and Second Author\thanks{For something else}\\
  Department of Informatics \\
  University of Some other Place
\and Third Author\thanksas{2}\\
  Department of Teleinformatics \\
  Nowhere University
\and Fourth Author\thanksas{1}\\
  Department of Applied Mathematics \\
  Somewere University of Technology}
\maketitle
\address{F. Author, Institute of Informatics, University of Somewhere}
\end{verbatim}

Running head must be defined with the two argument command
\verb|\runninghead| -- the first argument should contain the names of
the authors, second one should contain shortened version of title.
The issue number and year must be specified with help of the
\verb|\issue| command, and the starting page can be changed by setting
the value of the counter page to the proper value as in the following
example:

\begin{verbatim}
\setcounter{page}{1001}
\issue{XXI~(2001)}
\runninghead{F. Author, S. Author}{How to Prepare Articles for FI}
\end{verbatim}

The issue and page number must be specified before the
\verb|\maketitle| command. The running head must be defined on the
first page of the paper.

\subsection{Sections}

\LaTeX\ provides five levels of section headings and although
they are all defined
in the \texttt{fundam} style file, only the first four should be used.
\begin{itemize}
  \item Heading A -- \verb"\section";
  \item Heading B -- \verb"\subsection";
  \item Heading C -- \verb"\subsubsection";
  \item Heading D -- \verb"\paragraph";
  \item Heading E -- \verb"\subparagraph" should not be used.
\end{itemize}


\subsection{Lists}

The \texttt{fundam} class provides the three standard list
environments:
\begin{itemize}
  \item Numbered lists, created using the \texttt{enumerate}
environment.
  \item Unnumbered lists, created using the \texttt{itemize}
environment.
  \item Labelled lists, created using the \texttt{description}
environment.
\end{itemize}
The enumerated list numbers each list item with an arabic
numeral;
alternative styles can be achieved by inserting a redefinition
of the number
labeling command after the \verb!\begin{enumerate}!.
For example, a list numbered with roman numerals inside
parentheses can be
produced by the following commands:
\begin{verbatim}
    \begin{enumerate}
     \renewcommand\labelenumi{\theenumi}
     \renewcommand{\theenumi}{(\roman{enumi})}
     \item first item
     \item second item
     \item etc\dots
    \end{enumerate}
\end{verbatim}
This produces the following list:
\begin{enumerate}
  \renewcommand\labelenumi{\theenumi}
  \renewcommand{\theenumi}{(\roman{enumi})}
  \item first item
  \item second item
  \item etc\dots
\end{enumerate}

\subsection{Formul{\ae} and equations}

Standard \LaTeX\ environments (\texttt{equation} and
\texttt{eqnarray}) for typesetting equations can be used in the
articles. The numbering of equation is performed by
\LaTeX. These environments were used to typeset formulae (\ref{EQ:1}),
(\ref{EQ:2}) and (\ref{EQ:3})
\begin{equation}
\label{EQ:1}  a+b=c\ltag{*}
\end{equation}
\begin{eqnarray}
  a+b&=&c\ltag{(!)}\label{EQ:2}\\
  c-b&=&a\label{EQ:3}
\end{eqnarray}
Because the \texttt{amsmath} package is included by the
\texttt{fundam} class, the author can use the \AmS\TeX\ macros for
better handling of multi line formulae as shown in the examples
(\ref{EQ:4})--({\ref{EQ:7}}). For details refer to the standard
\AmS\LaTeX\ documentation.
\begin{align}
  a+b&=c\ltag{(a)}\label{EQ:4}\\
  c-b&=a\label{EQ:5}
\end{align}
\begin{gather}
  a+b=c\ltag{(*)}\label{EQ:6}\\
  c-b=a\ltag{(**)}\label{EQ:7}
\end{gather}

In addition to the standard numbering the equations can be marked with
extra tags placed on the left margin. The \verb|\ltag{}| command is
provided for such a purpose. The mandatory argument of this command
typeset in math mode is used as the tag. The equation (\ref{EQ:7}) was
for instance marked with command \verb|\ltag{(**)}|.

\subsection{Theorem like environments and proofs}

The \texttt{fundam} class defines ready to use environments for
typesetting typical theorem like environments: \texttt{definition},
\texttt{theorem}, \texttt{fact}, \texttt{lemma}, \texttt{example},
\texttt{assumption}, \texttt{proposition}, \texttt{remark},
\texttt{corollary}, \texttt{claim}. There is also defined the
\texttt{proof} environment. An example of using this environment is
given below (theorem \ref{trivial}):

\begin{verbatim}
\begin{theorem}[trivial]
There exist numbers $a$, $b$, and $c$, such that $2a+2b=2c$.
\end{theorem}
\begin{proof} We leave the proof for the reader.
\end{proof}
\end{verbatim}

\begin{theorem}[trivial]\label{trivial}
There exists numbers $a$, $b$, and $c$, such that $2a+2b=2c$.
\end{theorem}
\begin{proof} We leave the proof for the reader.
\end{proof}

You can define more theorem like environments if you need them using
the \verb!\newtheorem! command.

\subsection{Figures, tables, and algorithms}

The {\tt figure} and {\tt table} environments are inherited from the
standard article class as described in the \LaTeX\ Manual to provide
consecutively numbered floating inserts for illustrations and tables
respectively.

Figure captions should be below the figure itself, therefore the
\verb!\caption! command should appear after the figure or space left for
an illustration. For example, Figure~\ref{sample-figure} is produced
using the following commands:
\begin{verbatim}
  \begin{figure}
    ...
    \caption{An example figure with space for artwork.}
    \label{sample-figure}
  \end{figure}
\end{verbatim}

\begin{figure}[h]
\centerline{\setlength{\unitlength}{.4pt}%
\begin{picture}(400,100)(-200,-50)
\thinlines
\put(0,0){\oval(400,100)}
\put(0,0){\makebox(0,0)[b]{The simple figure}}
\end{picture}}
  \caption{An example figure with space for artwork.}
  \label{sample-figure}
\end{figure}

The preferred method of preparing figures is to save or convert them
to the Portable Document Format  (PDF) or a popular graphic format such as PNG (alternatively JPG, GIF). Such
pictures can be included in consistent way and easily scaled. The
\texttt{graphicx} package can be used to include
graphics into the paper. It provides the command
\verb|\includegraphics| as well as  an easy way of
resizing the picture using the optional, key-value parameters.

\begin{verbatim}
\includegraphics[width=0.5\textwidth]{file.pdf}
\end{verbatim}

The above example presets the use of \verb|\includegraphics| command
as provided by the \texttt{graphicx} package. The included picture
will be resized to the half width of the typesetting area. If you not
specify the height of the picture it will be scaled proportionally.

Table captions should be at the top therefore the \verb!\caption! command
should appear before the body of the table.
For example, Table~\ref{sample-table} is produced using the following
commands:

\begin{verbatim}
\begin{table}[htbp]\small
  \caption{An example table}
  \label{sample-table}
  \begin{center}
  \begin{tabular}{|c|c|c|}
\hline
$m$ & $z_\kappa$ & $s_\kappa$\\\hline
10& 1.13641 & 1.27700  \\\hline
20& 1.06601 & 1.27801  \\\hline
  \end{tabular}
\end{center}
\end{table}
\end{verbatim}

\begin{table}[htbp]\small
  \caption{An example table}
  \label{sample-table}
  \begin{center}
  \begin{tabular}{|c|c|c|}
\hline
$m$ & $z_\kappa$ & $s_\kappa$\\\hline
10& 1.13641 & 1.27700  \\\hline
20& 1.06601 & 1.27801  \\\hline
  \end{tabular}
\end{center}
\end{table}

The \texttt{tabular} environment should be used to produce ruled tables.
Commands to redefine quantities such as \verb!\arraystretch! should be
avoided.

\begin{algorithm}[h!] \label{ALG:1}\caption{Example -- {\sc Max} finds the maximum number}
\KwIn{A finite set $A=\{a_1, a_2, \ldots, a_n\}$ of integers}
\KwOut{The largest element in the set}
$max \gets a_1$\;
\For{$i \gets 2$ \textbf{to} $n$} {
  \If{$a_i > max$} {
    $max \gets a_i$\;
  }
}
\Return{$max$}\;

\label{algo:max}
\end{algorithm}


The (pseudo)code of algorithms should be placed in the \verb!algorithm! environment. We recommend the use of standard \verb!algorithm2e! package included in most  \LaTeXe  distributions.  By calling in the preamble \verb!\usepackage[ruled,lined]{algorithm2e}!  one can introduce an algorithm as shown in Algorithm \ref{ALG:1}.

%\begin{algorithm}
%\caption{Example of an algorithm}}
%	\KwIn{set of attributes $A$ and decision attribute $d$;\hfill\\ $\phi:A\times A\cup\{d\} \rightarrow R^{+}$
%		function for measuring dependency;\hfill\\ $N \in N$; $\varepsilon \in [0, 1)$;}
%	\KwOut{subset of attributes $A^{\prime} \subseteq A$}
%	\Begin{
%		\BlankLine
%		$A^{\prime} \gets \emptyset$;\\
%		$stopFlag \gets FALSE$;\\
%		$A^{\prime} \gets \arg\max_{a\in A}{\hspace{0.1cm}\phi\big(a, d\big)}$;\\
%		$A \gets A\setminus A^{\prime}$;\\
%		\While{$stopFlag == FALSE$}{
%			$\bar{a} \gets \arg\max_{a\in A}{\hspace{0.1cm}\Big(\phi\big(a, d\big) - \max_{b \in A^{\prime}}\hspace{0.1cm}\phi\big(a, b\big)\Big)}$;\\
%			\ForEach{$i \in 1, \ldots, N$}{
%				$\bar{p}_{i} \gets $ random permutation of $A$;\\
%			}
%			\eIf{ $\frac{|\{i : |\phi(\bar{p}_{i}, d)| > |\phi(\bar{a}, d)|\}| + 1}{N + 2} > \varepsilon$}{$stopFlag \gets TRUE$;}{$A^{\prime} \gets A^{\prime} \cup \bar{a}$}
%		}
%	}
%
%\end{algorithm}




\subsection{Bibliography}

Place citations as numbers in square brackets in the text. All publications cited in the text should be presented in a list of references at the end of the manuscript. List the references in the order in which they appear in the text. Only articles published or accepted for publication should be listed in the reference list. Submitted articles can be listed as (author(s), unpublished data). If an article has a DOI (Digital Object Identifier), this should be provided after the page number details. The number is added after the letters 'doi'. Manuscripts will not be considered if they do not conform to the Vancouver citation guidelines outlined above.

The best way to prepare bibliography for your paper is to use the
Bib\TeX\ program.  This will assure uniform citation format and provide means for better recognition of cited work. We assume that you know how to use the Bib\TeX\
program.  If not, consult the \LaTeX\ manual. The special Bib\TeX\
style \texttt{fundam.bst} is provided with the \LaTeX\ class. The  use of \texttt{fundam.bst} style assures compliance with Vancouver citation style. Assuming
that your Bib\TeX\ database file is named \texttt{citations.bib} you
should add the following two lines to your manuscript file. The
citations should be defined with \verb|\cite| command. Here we cite
the first two bibliographical entries \cite{RSFDGrC,KrasuskiJSS13}.

Authors are encouraged to include as much information in Bib\TeX\ records as possible. In particular the unique identifiers like DOI, arXiv number, URL or ISBN are useful.


\begin{verbatim}
\bibliographystyle{fundam}
\bibliography{citations}
\end{verbatim}

The manual formatting of your reference list is \textbf{highly discouraged}. We
strongly recommend that you ask your system personnel to install
Bib\TeX.

If you cannot use Bib\TeX for some reason, you must prepare your
bibliography manually, as the \verb!thebibiliography! environment.
For example, the bibliography of this guide can be produced manually
as follows:

\begin{small}
\begin{verbatim}
\begin{thebibliography}{1}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi:\discretionary{}{}{}#1}\else
  \providecommand{\doi}{doi:\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem{RSFDGrC}
{RSFDGrC}.
\newblock {International Conference on Rough Sets, Fuzzy Sets, Data Mining, and
  Granular-Soft Computing}.
\newblock \url{http://dblp.uni-trier.de/db/conf/rsfdgrc/index.html}.
\newblock Accessed: 2015-08-29.

\bibitem{KrasuskiJSS13}
Krasuski A, Jankowski A, Skowron A, \'{S}l\c{e}zak D.
\newblock {From Sensory Data to Decision Making: {A} Perspective on Supporting
  a Fire Commander}.
\newblock In: 2013 {IEEE/WIC/ACM} International Conferences on Web Intelligence
  and Intelligent Agent Technology, Atlanta, Georgia, USA, 17-20 November 2013,
  Workshop Proceedings \cite{DBLP:conf/iat/2013w}, 2013 pp. 229--236.
\newblock \doi{10.1109/WI-IAT.2013.188}.
\newblock
  \urlprefix\url{http://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=6690661}.

\bibitem{Wittgenstein}
Baker G, Hacker P.
\newblock Wittgenstein: Understanding and Meaning, Volume 1 of an Analytical
  Commentary on the Philosophical Investigations, Part II: Exegesis 1-184.
\newblock Wiley-Blackwell Publishing, 2004.
\newblock ISBN 9780470753101.
\newblock \doi{10.1002/9780470753101}.
\newblock 2nd Edition.

\bibitem{SzczukaSKK14}
Szczuka MS, Sosnowski L, Krasuski A, Krenski K.
\newblock Using Domain Knowledge in Initial Stages of {KDD:} Optimization of
  Compound Object Processing.
\newblock \emph{Fundamenta Informaticae}, 2014.
\newblock \textbf{129}(4):341--364.
\newblock \doi{10.3233/FI-2014-975}.

\bibitem{DBLP:conf/iat/2013w}
2013 {IEEE/WIC/ACM} International Conferences on Web Intelligence and
  Intelligent Agent Technology, Atlanta, Georgia, USA, 17-20 November 2013,
  Workshop Proceedings. {IEEE} Computer Society, 2013.
\newblock ISBN 978-1-4799-2902-3.
\newblock
  \urlprefix\url{http://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=6690661}.

\end{thebibliography}
\end{verbatim}
\end{small}


\nocite{*}
\bibliographystyle{fundam}
\bibliography{citations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}
